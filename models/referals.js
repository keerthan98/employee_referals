const Joi = require('joi');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const Status = new mongoose.Schema({
    isReferred : {
        type : Boolean,
        default : true
    },
    isAccepted : {
        type : Boolean,
        default : false
    },
    isInterviewed : {
        type : Boolean,
        default : false
    },
    isHired : {
        type : Boolean,
        default : false
    },
    notHired : {
        type : Boolean,
    }
}); 
 
const Referal = mongoose.model('Referal', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    qualification: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    job: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    referee_id: {
        type: Number,
        required: true,
        minlength: 5,
        maxlength: 10
    },
    referred_on:{
        type: Date
    },
   
    phone: {
        type: Number,
        required: true,
        minlength: 5,
        maxlength: 10
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    job_id : {
        type: String,
        required: true
    },
    department : {
        type :String,
        required : true
    },
    status : {type : Status, required : true}
}));
 
function validateUser(user) {
    const schema = {
        job_id : Joi.string().required(),
        name: Joi.string().min(5).max(50).required(),
        qualification: Joi.string().min(5).max(50).required(),
        job: Joi.string().min(5).max(50).required(),
        referee_id: Joi.number().integer().min(1000).max(99999999999999).required(),
        referred_on: Joi.date().required(),
        status : Joi.object({
            isAccepted : Joi.boolean().allow('', null).empty(['', null]).default('false').required(),
            isReferred : Joi.boolean().allow('', null).empty(['', null]).default('true').required(),
            isInterviewed : Joi.boolean().allow('', null).empty(['', null]).default('false').required(),
            isHired : Joi.boolean().allow('', null).empty(['', null]).default('false').required(),
            notHired : Joi.boolean().allow('', null).empty(['', null]).default('false').required()
        }).required(),
        email: Joi.string().min(5).max(255).required().email(),
        department: Joi.string().min(5).max(50).required(),
        phone: Joi.number().integer().min(1000000000).max(9999999999).required(),
    };
    return Joi.validate(user, schema);
}
 
exports.Referal = Referal;
exports.validate = validateUser;