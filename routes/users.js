const bcrypt = require("bcryptjs");
const _ = require('lodash');
const mongoose = require('mongoose');
const { User, validate } = require('../models/user');
const {Counter} = require('../models/counter');
const express = require('express');
const router = express.Router();
const fs = require('fs');
const config = require('config');
const jwt = require('jsonwebtoken');

mongoose.set('useFindAndModify',false);
 
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  

router.get('/:name', function(req, res) {
    var user = User.findOne({name:req.params.name}, (err, user) => {
        if(!err){
            if(user.usertype === "employee")
            {
                return res.status(200).send(user);
            }
            else{
                res.send({
                    message:"User doesnt exist"
                });
            }
        }
        

    });

  });

router.get('/',async (req, res)=>{
    // jwt.verify(req.token,'secretkey',(err,authdata)=>{
        // if(err){
        //     res.send(err);
        // }
        // else{
            var user = User.find({usertype:"employee"}, (err, user) => {
                if(err){
                    res.status(400).send(err);
                }else{
                    if(user.length > 0){
                        
                        res.send(user);
                    }else{
                        res.send( "no such document found");
                    }
                }
               
            });
           
        // }
    // });
   
    
    // res.status(200).send(user);
});

router.post('/userdelete', function(req, res){
    User.findOneAndRemove({_id : req.body._id}, function(err){
        if (!err) {
            res.send("user deleted");
        }
    else {
           res.send(err);
        }
    });
    });
 
router.post('/', async (req, res) => {
    //First Validate The Request
    const { error } = validate(req.body);
    if (error) {
         res.status(400).send(error.details[0].message);
    }
 
    // Check if this user already exisits
    let user = await User.findOne({ email: req.body.email });
    if (user) {
         res.status(400).send('That user already exisits!');
    } else {
            // Insert the new user if they do not exist yet
            // user = new User(_.pick(req.body, ['name', 'email', 'password','uid','usertype','phone','department','role','salary']));
            // var temp = getNextSequence();
            // console.log(temp)
            // user.uid = temp || 9089;
            Counter.findByIdAndUpdate({ _id: "uid" },{ $inc	: { "seq": 1 } }, {new: true},async function(err, data){
            if(err){
                res.send(err);
                // return Math.floor(Math.random * 199808099);
            } else {
                user = new User({
                    "name" : req.body.name,
                    "email" : req.body.email,
                "password" : req.body.password,
                "uid" : data.seq,
                "usertype" : req.body.usertype,
                "phone" : req.body.phone,
                "department" : req.body.department,
                "role" : req.body.role,
                "salary" :req.body.salary
                });

                const salt = await bcrypt.genSalt(10);
                user.password = await bcrypt.hash(user.password, salt);
                await user.save();
                res.status(200).json({data : "job added succesfully"});
                const token = jwt.sign({ _id: user._id }, config.get('PrivateKey'));
                res.header('x-auth-token', token).send(_.pick(user, ['_id', 'name', 'email']));
                

                

            }
            
        });
        
                
      
    
        
    }
});

function verifyToken(req,res,next){
    const bearerToken = req.header("Authorization");
    
    if(typeof bearerToken!=='undefined'){
        // const bearer = bearerHeader.split(' ');
        // const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else{
        res.sendStatus(403);
    }
}
 
module.exports = router;