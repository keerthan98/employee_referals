const _ = require('lodash');
const nodemailer=require('nodemailer');
const mongoose = require('mongoose');
const {Referal,validate} = require('../models/referals');
const {User} = require('../models/user');
const {Job} = require('../models/job');
const express = require('express');
const router = express.Router();


router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
router.post('/show_referals', (req, res)=>{
    
        
            var referee = Referal.find({department :req.body.department}, (err, referee) => {
                if(err){
                     res.status(400).send(err);
                }else{
                    res.status(200).send({data:referee});
                }
           
             });
      
        
    });

    router.post('/emp_referals', (req, res)=>{
        var filter = {};
        if(req.body.searchby == "department"){
            filter = {
                referee_id : req.body.referee_id,
                department : req.body.department
            }
        }else{
            filter = {referee_id : req.body.referee_id}
        }
        var referee = Referal.find(filter, (err, referee) => {
            if(err){
                 res.status(400).send(err);
            }else if(referee[0]){
                res.status(200).send({data:referee});
            }else{
                res.status(200).send({data:"No data"});
            }
       
         });
  
    
});

router.post('/update',async (req,res) =>{
    mongoose.set('useFindAndModify', false);
    Referal.findOneAndUpdate({_id: req.body._id}, {$set:{status:req.body.status}}, {new: true}, (err, doc) => {
        if (err) {
            // console.log("Something wrong when updating data!");
            res.status(400).send({data : err});
        }else{
            if(doc.status.isHired){
                Job.findOne({_id : doc.job_id}, (err, job) => {
                    if(err){
                        console.log(err);
                    }else{
                        // console.log(doc.referee_id);
                        User.findOneAndUpdate({uid : doc.referee_id}, {$inc : {referred : 1, points:job.referalbonus}}, {new : true}, (err, test) => {
                            // doc.save();
                            // console.log(doc.points);
                            if(err){
                                console.log(err);
                            }

                        });
                        // console.log(User.find({uid : doc.referee_id}));
                    }
                });
            }
            res.status(200).send({data : doc})
        }
    
        // console.log(doc);
    });

 
});

router.post('/addreferee',async (req, res) => {
    // First Validate The Request
    const { error } = validate(req.body);
    if (error) {
         res.status(400).send(error.details[0].message);
    }
    // Check if this referee already exisits
    var ref = await Referal.findOne({ email: req.body.email, job_id:req.body.job_id});
   
    if (ref) {
         res.status(400).send('That referee already exisits!');
    } else {
        // Insert the new referee if they do not exist yet
        ref = new Referal(_.pick(req.body, ['name', 'email','phone','qualification','job','status','referee_id','referred_on','job_id','department']));
        await ref.save();
        res.send("refeered");
        // res.send(_.pick(ref, ['_id','name', 'email','phone','qualification','job','referee_id','referred_on','status','job_id','department']));
    }
let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'drshnbp@gmail.com',
            pass:'291998@D'
        }
    });
      
      var mailOptions = {
        from: 'drshnbp@gmail.com',
        to: req.body.email,
        subject: ' u have selected  for survify ',
        text: 'That was easy!'
      };
      
      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
    
    
});

module.exports = router;