const bcrypt = require("bcryptjs");
const _ = require('lodash');
const { Job, validate } = require('../models/job');
const express = require('express');
const router = express.Router();
 

router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
router.post('/browsejob',(req,res)=>{
    var filter = {};
        if(req.body.searchby == "department"){
            filter = {
            
                department : req.body.department
            }

        }
        Job.find(filter, (err, job) => {
            if(err){
                 res.status(400).send(err);
            }else if(job[0]){
                res.status(200).send({data:job});
            }else{
                res.status(200).send({data:"No data"});
            }
       
         });
  
});

router.get('/', async (req,res) => {
    
    // if(req.query.searchby == null){
        Job.find({}, (err,job) => {
            if(!err){
                 res.status(200).send(job);
            }
            else{
                res.send(err)
            }
        });
    // }else if(req.query.searchby == 'role'){
    //     Job.find({role:{ $regex: new RegExp("^" + req.query.role.toLowerCase(), "i") }}, (err, job) => {
    //                  if(!err){
    //                       res.status(200).send({data: job});
    //                  }
    //     });

    // }else if(req.query.searchby == 'department'){
    //     Job.find({department:{ $regex: new RegExp("^" + req.query.department.toLowerCase(), "i") }}, (err, job) => {
    //         if(!err){
    //              res.status(200).send({data: job});
    //         }
    //     });
    // }

});
router.post('/jobdelete', function(req, res){
    Job.findOneAndRemove({_id : req.body._id}, function(err){
        if (!err) {
            res.send("job deleted");
        }
    else {
           res.send(err);
        }
    });
    });

 router.post('/', async (req, res) => {
        // First Validate The Request
        const { error } = validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }
     
        // Check if this user already exisits
       let user = await Job.findOne({ qualification:req.body.qualification,salary:req.body.salary,department:req.body.department,experience:req.body.experience,role:req.body.role});
       if (user) {
           res.status(400).send('That Job already exisits!');
        }
        else {
            
            // Insert the new user if they do not exist yet
            user = new Job(_.pick(req.body, ['qualification','salary','department','experience','role','referalbonus']));
           // res.status(200).send("job added succesfully");
            await user.save();
            res.json({data : "job added succesfully"});
           // res.status(200).send(_.pick(user, ['_id','qualification','salary','department','experience','role','referalbonus']));
           
        }
    });
 
module.exports = router;